﻿using ControleFinanceiro.BLL.Models;
using ControleFinanceiro.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ControleFinanceiro.DAL.Repositorios
{
    public class TipoRespositorio : RepositorioGenerico<Tipo>, ITipoRepositorio
    {
        public TipoRespositorio(Contexto contexto) : base(contexto)
        {
        }
    }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AtualizarCategoriaComponent } from './componentes/Categoria/atualizar-categoria/atualizar-categoria.component';
import { ListagemCategoriasComponent } from './componentes/Categoria/listagem-categorias/listagem-categorias.component';
import { NovaCategoriaComponent } from './componentes/Categoria/nova-categoria/nova-categoria.component';
import { DashboardComponent } from './componentes/Dashboard/dashboard/dashboard.component';
import { AtualizarFuncaoComponent } from './componentes/Funcao/atualizar-funcao/atualizar-funcao.component';
import { ListagemFuncoesComponent } from './componentes/Funcao/listagem-funcoes/listagem-funcoes.component';
import { NovaFuncaoComponent } from './componentes/Funcao/nova-funcao/nova-funcao.component';
import { LoginUsuarioComponent } from './componentes/Usuario/Login/login-usuario/login-usuario.component';
import { RegistrarUsuarioComponent } from './componentes/Usuario/Registro/registrar-usuario/registrar-usuario.component';
import { AuthGuardService } from './services/auth-guard.service';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path : 'categorias/listagemcategorias', component: ListagemCategoriasComponent
      },
      {
        path : 'categorias/novacategoria', component: NovaCategoriaComponent
      },
      {
        path : 'categorias/atualizarcategoria/:id', component: AtualizarCategoriaComponent
      },
      {
        path : 'funcoes/listagemfuncoes', component: ListagemFuncoesComponent
      },
      {
        path : 'funcoes/novafuncao', component: NovaFuncaoComponent
      },
      {
        path : 'funcoes/atualizarfuncao/:id', component: AtualizarFuncaoComponent
      },
    ]
  },

  {
    path : 'usuarios/registrarusuario', component: RegistrarUsuarioComponent
  },
  {
    path : 'usuarios/loginusuario', component: LoginUsuarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
